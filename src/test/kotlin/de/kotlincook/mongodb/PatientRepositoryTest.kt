package de.kotlincook.mongodb

import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.mongo.embedded.EmbeddedMongoAutoConfiguration
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.testcontainers.containers.MongoDBContainer
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.junit.jupiter.Testcontainers

@Testcontainers
@DataMongoTest(excludeAutoConfiguration = [EmbeddedMongoAutoConfiguration::class])
class PatientRepositoryTest {

    companion object {
        @Container
        var mongoDBContainer = MongoDBContainer("mongo:5.0.5")

        @JvmStatic
        @DynamicPropertySource
        fun setProperties(registry: DynamicPropertyRegistry) {
            registry.add("spring.data.mongodb.uri", mongoDBContainer::getConnectionString)
            registry.add("spring.data.mongodb.database") { "mongo-test-database" }
        }
    }

    @Autowired
    private lateinit var patientRepository: PatientRepository

    @AfterEach
    fun cleanUp() {
        patientRepository.deleteAll()
    }

    @Test
    fun shouldReturnListOfPatients() {
        patientRepository.save(Patient(name = "Angelika", description = "Ehefrau"))
        patientRepository.save(Patient(name = "Magdalena", description = "Tochter"))
        val patients: List<Patient> = patientRepository.findAll()
        Assertions.assertEquals(2, patients.size)
    }

}